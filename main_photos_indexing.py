import sqlite3
import os
from pathlib import Path
from argparse import ArgumentParser
from tqdm import trange
from PIL import Image


def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--baseDir", required=True)
    parser.add_argument("--sqliteFilePath", required=True)
    parser.add_argument("--publicFilesDir", required=True)
    parser.add_argument("--createThumbnails", required=False, type=int)
    args = parser.parse_args()
    args.baseDir = Path(args.baseDir).absolute()
    args.sqliteFilePath = Path(args.sqliteFilePath).absolute()
    if args.publicFilesDir is None:
        print("[getArgs] Public Files Dir not set. Setting to baseDir='%s'" % args.baseDir)
        args.publicFilesDir = args.baseDir
    args.publicFilesDir = Path(args.publicFilesDir).absolute()
    args.createThumbnails = bool(args.createThumbnails) if not args.createThumbnails is None else False
    return args

def main():
    args = getArgs()
    print("[main] Base dir: %s" % args.baseDir)
    print("[main] Sqlite DB path: %s" % args.sqliteFilePath)

    suffixes = [".png", ".jpg"]
    fileNames = [x.name for x in Path(args.baseDir).glob("*") if x.suffix in suffixes]
    assert len(fileNames) > 0

    connection = sqlite3.connect(args.sqliteFilePath)
    cursor = connection.cursor()

    albumName = args.baseDir.name
    print("[main] Inserting album '%s' to the DB." % albumName)
    query = "INSERT INTO photos_album ('authorId', 'albumTitle') VALUES ('1', '%s')" % albumName
    print("[main] Query: %s" % query)
    cursor.execute(query)
    connection.commit()

    query = "SELECT * FROM photos_album ORDER BY id DESC LIMIT 1"
    cursor.execute(query)
    items = cursor.fetchall()
    assert len(items) == 1
    id = items[0][0]
    print("[main] Inserting all found photos (%d) to album with id %d" % (len(fileNames), id))

    # id = 22
    albumDirPath = args.publicFilesDir / ("photos/%s" % id)
    if not albumDirPath.exists():
        print("[main] Creating symlink '%s' to '%s'" % (args.baseDir, albumDirPath)) 
        os.symlink(args.baseDir, albumDirPath)

    for fileName in fileNames:
        query = "INSERT into photos ('albumId', 'photoFile', 'photoTitle', 'photoDescription') VALUES ('%d', '%s', 'n/a', 'n/a')" % (id, fileName)
        cursor.execute(query)
    connection.commit()
    print("[main] Added all files to the album.")

    print(fileNames)
    if args.createThumbnails:
            print("[main] Creating thumbnails of 300px width.")
            Path(albumDirPath / "thumbnails").mkdir(exist_ok=True)
            for i in trange(len(fileNames)):
                inFilePath = Path(albumDirPath / fileNames[i])
                outFilePath = Path(albumDirPath / "thumbnails" / fileNames[i])
                if outFilePath.exists():
                    continue
                img = Image.open(inFilePath)
                width = 300 
                height = int(width * img.height / img.width) 
                resizedImage = img.resize(size=(width, height), resample=Image.BICUBIC)
                resizedImage.save(str(outFilePath))

if __name__ == "__main__":
    main()
